//EXERCÍCIO 1
import readline from 'readline-sync';

console.log("Versão com FOR");
let início: number= readline.questionInt('Digite um valor de início do intervalo: ');
let fim: number=  readline.questionInt('Digite um valor de fim do intervalo: ');
for (let i=início; i<fim; i++) {
  if (i%2==0){
    console.log(i);
  }
}

console.log("Versão com WHILE");
let início2: number= readline.questionInt('Digite um valor de início do intervalo: ');
let fim2: number=  readline.questionInt('Digite um valor de fim do intervalo: ');

while (início2 <= fim2){
  if (início2%2===0){
    console.log(início2);
  }
  início2++;
}


